package filepathhelper;

import java.nio.file.Paths;

public class App 
{
	public String makeAbsolute(final String relativePath) {
		String absolutePath = "";
		if (relativePath.contains("~")) {
			absolutePath = resolveHomeDirectory(relativePath);
		} else if (relativePath.contains("\\..\\")) {
			absolutePath = resolvePlaceholder(relativePath);
		} else if (relativePath.contains(".\\")) {
			absolutePath = resolveWorkingDirectory(relativePath);
		}
		return absolutePath;
	}

	public String resolveHomeDirectory(String relativePath) {
		final String homeDirectory = System.getProperty("user.home");
		relativePath = relativePath.substring(1); // remove ~
		return homeDirectory + relativePath;
	}

	public String resolveWorkingDirectory(String relativePath) {
		final String workingDirectory = System.getProperty("user.dir");
		relativePath = relativePath.substring(1); // remove .
		return workingDirectory + relativePath;
	}

	public String resolvePlaceholder(final String relativePath) {
		return Paths.get(relativePath).toAbsolutePath().normalize().toString();
	}
}
