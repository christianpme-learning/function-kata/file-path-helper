package filepathhelper;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AppTest 
{
    public String[] getRelativeTestPaths(){
        return new String []{
            ".\\src\\test\\java\\filepathhelper\\resources\\config.json",
            "~\\Pictures\\file-path-helper\\mountains.jpg",
            "\\Users\\chris\\Desktop\\Workspace\\Katas\\file-path-helper\\src\\test\\java\\filepathhelper\\resources\\testfiles\\..\\program.cs"
        };
    }

    public String[] getAbsoluteTestPaths(){
        return new String[]{
            "C:\\Users\\chris\\Desktop\\Workspace\\Katas\\file-path-helper\\src\\test\\java\\filepathhelper\\resources\\config.json",
            "C:\\Users\\chris\\Pictures\\file-path-helper\\mountains.jpg",
            "C:\\Users\\chris\\Desktop\\Workspace\\Katas\\file-path-helper\\src\\test\\java\\filepathhelper\\resources\\program.cs"
        };
    }
    
    @Test
    public void makeAbsoluteTest(){  
        final App app = new App();

        final String[] relatives = getRelativeTestPaths();
        for (int i = 0; i < relatives.length; ++i) {

            final String actual = app.makeAbsolute(relatives[i]);
            final String expected = getAbsoluteTestPaths()[i];
            final boolean result = expected.equals(actual);
            assertTrue(result);
        }
    }

    @Test
    public void resolveHomeDirectoryTest() {
        final App app = new App();

        final String relativePath = getRelativeTestPaths()[1];

        final String expected = getAbsoluteTestPaths()[1];

        final String actual = app.resolveHomeDirectory(relativePath);

        assertTrue(expected.equals(actual));
    }

    @Test
    public void resolveWorkingDirectoryTest() {
        final App app = new App();

        final String relativePath = getRelativeTestPaths()[0];

        final String expected = getAbsoluteTestPaths()[0];

        final String actual = app.resolveWorkingDirectory(relativePath);

        assertTrue(expected.equals(actual));
    }

    @Test
    public void resolvePlaceholderTest() {
        final App app = new App();

        final String relativePath = getRelativeTestPaths()[2];

        final String expected = getAbsoluteTestPaths()[2];

        final String actual = app.resolvePlaceholder(relativePath);

        assertTrue(expected.equals(actual));
    }
    
}
